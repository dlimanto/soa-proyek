const express    = require('express');
const router     = express.Router();
const mysql      = require('mysql');
const config     = require('./config');
const jwtChecker = require('./tokenChecker');
const keyChecker = require('./keyChecker');

router.use(keyChecker);
router.use(jwtChecker);
/**
 * API untuk menambahkan satuan unit barang
 * @link    localhost:3000/api/unit/add
 * @method  POST
 * 
 * @return  JSON
 */
router.post('/add', (req,res) => {
    let postData = req.body;
    let pool     = mysql.createPool(config.mysql);
    let query    = "INSERT INTO unit_ukur(nama_unit,basic_unit,base_unit,qty_base_unit) VALUES(?,?,?,?)";
    let table    = [ postData.nama, postData.basic, postData.base, postData.qty ];
    query        = mysql.format(query, table);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "message" : "Berhasil menambahkan unit ukur" });
        }
    });
});

/**
 * API untuk mengubah data unit ukur
 * @link    localhost:3000/api/unit/update
 * @method  PUT
 * 
 * @return  JSON
 */
router.put('/update', (req,res) => {
    let postData = req.body;
    let pool     = mysql.createPool(config.mysql);
    let query    = "UPDATE unit_ukur SET nama_unit = ?, basic_unit = ?, base_unit = ?, qty_base_unit = ? WHERE id_unit = ?";
    let table    = [ postData.nama, postData.basic, postData.base, postData.qty, postData.id ];
    query        = mysql.format(query, table);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Email telah dipakai" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "message" : "Berhasil mengubah unit ukur" });
        }
    });
});

/**
 * API untuk mendapatkan seluruh data unit ukur
 * @link    localhost:3000/api/unit/get?limit=:limit&offset=:offset
 * @method  GET
 * 
 * @return  JSON
 */
router.get('/get', (req,res) => {
    let pool   = mysql.createPool(config.mysql);
    let limit  = req.query.limit;
    let offset = req.query.offset;
    let query  = "SELECT * FROM unit_ukur LIMIT ?, ?";
    if(limit  == null || offset == null){
        query  = "SELECT * FROM unit_ukur";
    }else{
        query  = mysql.format(query, [ parseInt(offset), parseInt(limit) ]);
    }
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "value" : rows });
        }
    });
});

/**
 * API untuk mendapatkan detail data unit ukur
 * @link    localhost:3000/api/unit/detail/:id
 * @method  GET
 * 
 * @return  JSON
 */
router.get('/detail/:id', (req,res) => {
    let id    = req.params.id;
    let pool  = mysql.createPool(config.mysql);
    let query = "SELECT * FROM unit_ukur WHERE id_unit = ?";
    query     = mysql.format(query, [ id ]);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "value" : rows });
        }
    });
});

/**
 * API untuk menkonversi non base unit ke base unit
 * @link    localhost:3000/api/unit/conversion/base/:id?qty=:qty
 * @method  GET
 * 
 * @return  JSON
 */
router.get('/conversion/base/:id', (req,res) => {
    let id    = req.params.id;
    let pool  = mysql.createPool(config.mysql);
    let query = "SELECT * FROM unit_ukur WHERE id_unit = ?";
    query     = mysql.format(query, [ id ]);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }
        if(rows){
            let basic = rows[0].basic_unit;
            if(basic == "1"){
                res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Satuan unit adalah satuan unit dasar" });
            }else{
                let qty = req.query.qty;
                if(qty == null){
                    res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Jumlah tidak boleh null" });
                }else{
                    let conversion = qty * rows[0].qty_base_unit;
                    let query2     = "SELECT nama_unit FROM unit_ukur WHERE id_unit = ?";
                    query2         = mysql.format(query2, [ rows[0].base_unit ]);
                    pool.query(query2, (e, r) => {
                        if(e){
                            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
                        }
                        if(r){
                            let response = {
                                "code"   : "200",
                                "status" : "Success",
                                "value"  : {
                                    "qty"         : qty,
                                    "unit"        : rows[0].nama_unit,
                                    "qty_convert" : conversion,
                                    "base_unit"   : r[0].nama_unit
                                }
                            };
                            res.status(200).json(response);
                        }else{
                            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Unit tidak memiliki satuan unit dasar" });
                        }
                    });
                }
            }
        }else{
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }
    });
});

module.exports = router;