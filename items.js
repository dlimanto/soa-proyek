const express    = require('express');
const router     = express.Router();
const mysql      = require('mysql');
const config     = require('./config');
const jwtChecker = require('./tokenChecker');
const keyChecker = require('./keyChecker');

router.use(keyChecker);
router.use(jwtChecker);
/**
 * API untuk menambahkan data item
 * @link    localhost:3000/api/items/add
 * @method  POST
 * 
 * @return  JSON
 */
router.post('/add', (req,res) => {
    let postData = req.body;
    let pool     = mysql.createPool(config.mysql);
    let query    = "INSERT INTO items(nama_item,brand_item,category_item,supplier_item,qty_item,unit_item,location_item,status_item) VALUES(?,?,?,?,?,?,?,?)";
    let table    = [ postData.nama, postData.brand, postData.category, postData.supplier, postData.qty, postData.unit, postData.location, "1" ];
    query        = mysql.format(query, table);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "message" : "Berhasil menambahkan barang" });
        }
    });
});

/**
 * API untuk mengubah data item
 * @link    localhost:3000/api/items/update
 * @method  PUT
 * 
 * @return  JSON
 */
router.put('/update', (req,res) => {
    let postData = req.body;
    let pool     = mysql.createPool(config.mysql);
    let query    = "UPDATE items SET nama_item = ?, brand_item = ?, category_item = ?, supplier_item = ?, qty_item = ?, unit_item = ?, location_item = ?, status_item = ? WHERE id_item = ?";
    let table    = [ postData.nama, postData.brand, postData.category, postData.supplier, postData.qty, postData.unit, postData.location, postData.status, postData.id ];
    query        = mysql.format(query, table);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Email telah dipakai" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "message" : "Berhasil mengubah barang" });
        }
    });
});

/**
 * API untuk menghapus data barang
 * @link    localhost:3000/api/items/delete/:id
 * @method  PUT
 * 
 * @return  JSON
 */
router.put('/delete/:id', (req,res) => {
    let id       = req.params.id;
    let pool     = mysql.createPool(config.mysql);
    let query    = "UPDATE items SET status_item = '0' WHERE id_item = ?";
    let table    = [ id ];
    query        = mysql.format(query, table);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "message" : "Berhasil menghapus barang" });
        }
    });
});

/**
 * API untuk mendapatkan seluruh data barnag
 * @link    localhost:3000/api/items/get?limit=:limit&offset=:offset
 * @method  GET
 * 
 * @return  JSON
 */
router.get('/get', (req,res) => {
    let pool   = mysql.createPool(config.mysql);
    let limit  = req.query.limit;
    let offset = req.query.offset;
    let query  = "SELECT * FROM items LIMIT ?, ?";
    if(limit  == null || offset == null){
        query  = "SELECT * FROM items";
    }else{
        query  = mysql.format(query, [ parseInt(offset), parseInt(limit) ]);
    }
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "value" : rows });
        }
    });
});

/**
 * API untuk mendapatkan detail data barang
 * @link    localhost:3000/api/items/detail/:id
 * @method  GET
 * 
 * @return  JSON
 */
router.get('/detail/:id', (req,res) => {
    let id    = req.params.id;
    let pool  = mysql.createPool(config.mysql);
    let query = "SELECT * FROM items WHERE id_item = ?";
    query     = mysql.format(query, [ id ]);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "value" : rows });
        }
    });
});

module.exports = router;