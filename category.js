const express    = require('express');
const router     = express.Router();
const mysql      = require('mysql');
const config     = require('./config');
const jwtChecker = require('./tokenChecker');
const keyChecker = require('./keyChecker');

router.use(keyChecker);
router.use(jwtChecker);
//router.use(require('./tokenChecker'));
/**
 * API untuk menambahkan category
 * @link    localhost:3000/api/category/add
 * @method  POST
 * 
 * @return  JSON
 */
router.post('/add', (req,res) => {
    let postData = req.body;
    let pool     = mysql.createPool(config.mysql);
    let query    = "INSERT INTO category(nama_category,status_category) VALUES(?,?)";
    let table    = [ postData.nama, '1' ];
    query        = mysql.format(query, table);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "message" : "Berhasil menambahkan kategori" });
        }
    });
});

/**
 * API untuk mengubuah nama category
 * @link    localhost:3000/api/category/update
 * @method  PUT
 * 
 * @return  JSON
 */
router.put('/update', (req,res) => {
    let postData = req.body;
    let pool     = mysql.createPool(config.mysql);
    let query    = "UPDATE category SET nama_category = ?, status_category = ? WHERE id_category = ?";
    let table    = [ postData.nama, postData.status, postData.id ];
    query        = mysql.format(query, table);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Email telah dipakai" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "message" : "Berhasil mengubah kategori" });
        }
    });
});

/**
 * API untuk menghapus category
 * @link    localhost:3000/api/category/delete
 * @method  PUT
 * 
 * @return  JSON
 */
router.put('/delete/:id', (req,res) => {
    let id       = req.params.id;
    let pool     = mysql.createPool(config.mysql);
    let query    = "UPDATE category SET status_category = '0' WHERE id_category = ?";
    let table    = [ id ];
    query        = mysql.format(query, table);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "message" : "Berhasil menghapus kategori" });
        }
    });
});

/**
 * API untuk mendapatkan seluruh data kategori
 * @link    localhost:3000/api/category/get
 * @method  GET
 * 
 * @return  JSON
 */
router.get('/get', (req,res) => {
    let pool   = mysql.createPool(config.mysql);
    let limit  = req.query.limit;
    let offset = req.query.offset;
    let query  = "SELECT id_category, nama_category FROM category WHERE status_category = '1' LIMIT ?, ?";
    if(limit  == null || offset == null){
        query  = "SELECT id_category, nama_category FROM category WHERE status_category = '1'";
    }else{
        query  = mysql.format(query, [ parseInt(offset), parseInt(limit) ]);
    }
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "value" : rows });
        }
    });
});

/**
 * API untuk mendapatkan detail data kategori
 * @link    localhost:3000/api/category/detail/:id
 * @method  GET
 * 
 * @return  JSON
 */
router.get('/detail/:id', (req,res) => {
    let id    = req.params.id;
    let pool  = mysql.createPool(config.mysql);
    let query = "SELECT id_category, nama_category, status_category FROM category WHERE id_category = ?";
    query     = mysql.format(query, [ id ]);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "value" : rows });
        }
    });
});

module.exports = router;