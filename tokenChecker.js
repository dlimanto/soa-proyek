const jwt    = require('jsonwebtoken');
const config = require('./config');

module.exports  = (req, res, next) => {
    const token = req.body.token || req.query.token || req.headers['x-access-token'];

    if(token){
        jwt.verify(token, config.secret, function(err, decoded){
            if(err){
                console.log(err.message);
                return res.status(403).json({
                    "code" : "403", 
                    "status" : "Error",
                    "message" : "Unauthorized"
                });
            }
            req.decoded = decoded;
            next();
        });
    }else{
        return res.status(403).send({
            "error" : "403",
            "status" : "Error",
            "message" : "Unauthorized"
        });
    }
}