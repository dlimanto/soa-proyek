const mysql      = require('mysql');
const config     = require('./config');

module.exports = (req, res, next) => {
    let key    = req.body.key || req.query.key || req.headers['api-key'];
    if(key){
        let pool   = mysql.createPool(config.mysql);
        let sql    = "SELECT api_key FROM user WHERE id_user = ?";
        let temp   = key.split('-');
        let idUser = temp[0];
        key        = temp[1];
        sql        = mysql.format(sql, [ idUser ]);
        pool.query(sql, (err, rows) => {
            if(err){
                return res.status(403).send({
                    "error" : "403",
                    "status" : "Error",
                    "message" : "Unauthorized"
                });
            }
            if(rows){
                let apiKey = rows[0].api_key;
                if(apiKey == key){
                    next();
                }else{
                    return res.status(403).send({
                        "error" : "403",
                        "status" : "Error",
                        "message" : "Unauthorized"
                    });
                }
            }else{
                return res.status(403).send({
                    "error" : "403",
                    "status" : "Error",
                    "message" : "Unauthorized"
                });
            }
        })
    }else{
        return res.status(403).send({
            "error" : "403",
            "status" : "Error",
            "message" : "Unauthorized"
        });
    }
}