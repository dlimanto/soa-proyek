const express    = require('express');
const router     = express.Router();
const mysql      = require('mysql');
const config     = require('./config');
const jwtChecker = require('./tokenChecker');
const keyChecker = require('./keyChecker');

/**
 * API untuk melakukan register
 * @link    localhost:3000/api/users/register
 * @method  POST
 * 
 * @return  JSON
 */
router.post('/register', (req, res) => {
    let bcrypt   = require('bcryptjs');
    let postData = req.body;
    let pool     = mysql.createPool(config.mysql);
    let sql      = "SELECT * FROM user WHERE email_user=?";
    let email    = [ postData.email ];
    sql          = mysql.format(sql, email);
    pool.query(sql, (err, rows) => {
        let respon = { "code"    : "400", "status"  : "Error", "message" : "Email telah dipakai" };
        if(err){
            respon.message = err.message;
            res.status(400).json(respon);
        }
        if(rows){
            if(rows.length > 0){
                res.status(400).json(respon);
            }else{
                let key      = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","1","2","3","4","5","6","7","8","9","0"];
                let apiKey   = "";
                for(let i = 0;i < 50;i++){
                    let index = Math.floor(Math.random() * 36);
                    if(index < 26){
                        let upper = Math.floor(Math.random() * 2);
                        if(upper < 1) apiKey += (key[index]).toUpperCase();
                        else apiKey += key[index];
                    }else apiKey    += key[index];
                }
                let query = "INSERT INTO user(username_user,nama_user,alamat_user,no_user,password_user,api_key,status_user,email_user) VALUES(?,?,?,?,?,?,?,?)";
                let table = [
                    postData.username,
                    postData.nama,
                    postData.alamat,
                    postData.no,
                    bcrypt.hashSync(postData.password),
                    apiKey,
                    '1',
                    postData.email
                ];
                query    = mysql.format(query, table);
                pool.query(query, (err, rows) => {
                    let response = {
                        "code"    : "code",
                        "status"  : "Error",
                        "message" : "message"
                    };
                    if(err){
                        response.code    = "400";  
                        response.message = err.message;
                        res.status(400).json(response);
                    }else{
                        response.code    = "200";
                        response.message = "Berhasil register";
                        response.status  = "Success";
                        res.status(200).json(response);
                    }
                });
            }
        }
    });
}); 

/**
 * API untuk melakukan login
 * @link    localhost:3000/api/users/login
 * @method  POST
 * 
 * @return  JSON
 */
router.post('/login', (req, res) => {
    let jwt      = require('jsonwebtoken');
    let bcrypt   = require('bcryptjs');
    let postData = req.body;
    let pool     = mysql.createPool(config.mysql);
    let query    = "SELECT * FROM user WHERE email_user=?";
    let table    = [ postData.email ];
    query        = mysql.format(query, table);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code"    : "400", "status"  : "Error", "message" : err.message });
        }
        if(rows){
            let user = rows[0];
            bcrypt.compare(postData.password, user.password_user).then((e) => {
                if(e){
                    let u = { "email"    : postData.email, "password" : postData.password };
                    let response = {
                        "code"   : "200",
                        "status" : "Success",
                        "value"  : {
                            "id"           : user.id_user,
                            "username"     : user.username_user,
                            "nama"         : user.nama_user,
                            "email"        : user.email_user,
                            "alamat"       : user.alamat_user,
                            "nomor"        : user.no_user,
                            "key"          : user.api_key + '-' + user.id_user,
                            "token"        : jwt.sign(u, config.secret, { expiresIn : config.tokenLife }),
                            "refreshToken" : jwt.sign(u, config.refreshTokenSecret, { expiresIn: config.refreshTokenLife })
                        }
                    };
                    res.status(200).json(response);
                }else{
                    res.status(400).json({ "code"    : "400", "status"  : "Error", "message" : "Password salah" });
                }
            });
        }else{
            res.status(400).json({ "code"    : "400", "status"  : "Error", "message" : "Email tidak terdaftar" });
        }
    });
});

router.use(keyChecker);
router.use(jwtChecker);
/**
 * API untuk mengubah data profile user
 * @link    localhost:3000/api/users/update
 * @method  PUT
 * 
 * @return  JSON
 */
router.put('/update', (req,res) => {
    let postData = req.body;
    let pool     = mysql.createPool(config.mysql);
    let query    = "UPDATE user SET username_user = ?, nama_user = ?, alamat_user = ?, no_user = ?, password_user = ? WHERE email_user = ?";
    let table    = [ postData.username, postData.nama, postData.alamat, postData.no, bcrypt.hashSync(postData.password), postData.email ];
    query        = mysql.format(query, table);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Email telah dipakai" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "message" : "Berhasil mengubah profil user" });
        }
    });
});

/**
 * API untuk mendapatkan detail user
 * @link    localhost:3000/api/users/detail?id=:id || localhost:3000/api/users/detail?email=:email
 * @method  PUT
 * 
 * @return  JSON
 */
router.get('/detail', (req,res) => {
    let where = req.query.id;
    let pool  = mysql.createPool(config.mysql);
    if(where == null){
        where = req.query.email;
    }
    if(where == null){
        res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Tidak ada user yang dipilih" });
    }else{
        let query = "SELECT email_user, username_user, nama_user, alamat_user, no_user FROM user WHERE ";
        if(req.query.id == null){
            query += "email_user = ?";
        }else{
            query += "id_user = ?"; 
        }
        query     = mysql.format(query, [ where ]);
        pool.query(query, (err, rows) => {
            if(err){
                res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Email telah dipakai" });
            }
            if(rows){
                res.status(200).json({ "code" : "200", "status"  : "Success", "value" : rows[0] });
            }
        });
    }
    
});

module.exports = router;