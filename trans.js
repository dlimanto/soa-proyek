const express    = require('express');
const router     = express.Router();
const mysql      = require('mysql');
const config     = require('./config');
const jwtChecker = require('./tokenChecker');
const keyChecker = require('./keyChecker');

router.use(keyChecker);
router.use(jwtChecker);
/**
 * API untuk menambahkan data transaksi
 * @link    localhost:3000/api/trans/add/:mode
 * @method  POST
 * 
 * @return  JSON
 */
router.post('/add/:mode', (req,res) => {
    let postData = req.body;
    let mode     = req.params.mode;
    if(mode == "in"){
        mode = 1;
    }else{
        mode = 0;
    }
    let idTrans = "TR";
    if(mode == 1){
        idTrans += "IN";
    }else{
        idTrans += "OT";
    } 
    let tgl  = new Date(postData.tanggal);
    if(tgl.getDate() < 10) idTrans += "0";
    idTrans += tgl.getDate().toString();
    if((tgl.getMonth() + 1) < 10) idTrans += "0";
    idTrans += (tgl.getMonth() + 1).toString();
    idTrans += tgl.getFullYear().toString();
    let jmlBarang = postData.items.length;
    let pool      = mysql.createPool(config.mysql);
    let query     = "SELECT COUNT(*) as jml FROM h_trans WHERE id_trans LIKE '%" + idTrans + "%'";
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }
        if(rows){
            let jml = rows[0].jml + 1;
            if(jml < 10) idTrans += "00" + jml;
            else if(jml < 100) idTrans += "0" + jml;
            else idTrans += jml;
            query     = "INSERT INTO h_trans VALUES(?,?,?,?,?,?)";
            let table = [ idTrans, tgl, mode, postData.user, postData.location, jmlBarang ];
            query     = mysql.format(query, table);
            pool.query(query, (err, rows) => {
                if(err){
                    res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
                }else{
                    let items = postData.items;
                    for(let i = 0;i < items.length;i++){
                        query = "INSERT INTO d_trans VALUES(?,?,?)";
                        query = mysql.format(query, [ idTrans, items[i].id, items[i].qty ]);
                        pool.query(query, (err, rows) => {});
                    }
                    res.status(400).json({ "code" : "200", "status"  : "Success", "message" : "Berhasil menambahkan transaksi" });
                }
            });
        }
    });
});

/**
 * API untuk mendapatkan seluruh data transaksi
 * @link    localhost:3000/api/traans/get?limit=:limit&offset=:offset
 * @method  GET
 * 
 * @return  JSON
 */
router.get('/get', (req,res) => {
    let pool   = mysql.createPool(config.mysql);
    let limit  = req.query.limit;
    let offset = req.query.offset;
    let query  = "SELECT id_trans, tgl_trans, mode_trans, jml_trans, nama_user, nama_location FROM h_trans,user,location WHERE user_trans = id_user AND lokasi_trans = id_location LIMIT ?, ?";
    if(limit  == null || offset == null){
        query  = "SELECT id_trans, tgl_trans, mode_trans, jml_trans, nama_user, nama_location FROM h_trans,user,location WHERE user_trans = id_user AND lokasi_trans = id_location";
    }else{
        query  = mysql.format(query, [ parseInt(offset), parseInt(limit) ]);
    }
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "value" : rows });
        }
    });
});

/**
 * API untuk mendapatkan detail data transaksi
 * @link    localhost:3000/api/trans/detail/:id
 * @method  GET
 * 
 * @return  JSON
 */
router.get('/detail/:id', (req,res) => {
    let id    = req.params.id;
    let pool  = mysql.createPool(config.mysql);
    let query = "SELECT id_trans, nama_item, item_qty,nama_unit FROM d_trans,items,unit_ukur WHERE id_trans = ? AND d_trans.id_item = items.id_item AND unit_item = id_unit";
    query     = mysql.format(query, [ id ]);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "value" : rows });
        }
    });
});

module.exports = router;